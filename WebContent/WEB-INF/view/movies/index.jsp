<%@page import="java.util.List"%>
<%@page import="java.util.ArrayList"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ page import="model.Movie"%>
<%@ include file = "../header.jsp" %>
<%! List <Movie> listBase;%>

<%
	//Para las sesiones sería así
  //list = (List<Book>)session.getAttribute("book");
	listBase = (List<Movie>)request.getAttribute("list");
  if (listBase == null) {
	  listBase = new ArrayList<Movie>();
  }else{
%>
	  <table>
	  <tr>
	  	<th>id</th>
	  	<th>title</th>
	  	<th>Director</th>
	  	<th>year</th>
	  	<th>acciones</th>
	  </tr>
	  <% for (Movie item : listBase) {%>
	    <tr>
	    <td><%= item.getId() %></td>
	    <td><%= item.getTitle() %></td>
	    <td><%= item.getDirector() %></td>
	    <td><%= item.getYear() %></td>
	    <td>
	    	<a href="<%= request.getContextPath() %>/movies/index<%= item.getId() %>">ver</a>
	    	<a href="<%= request.getContextPath() %>/movies/<%= item.getId() %>/delete">borrar</a>
	    	<a href="<%= request.getContextPath() %>/movies/<%= item.getId() %>/remember">recordar</a>
	    </td>
	    </tr>
	  <% } %>
	  </table>
<%
  }
%>
<br><br>
<a href="/prepExamen/movies/create">crear</a>
<a href="/prepExamen/movies/index">volver</a>

<%@ include file = "../footer.jsp" %>