package servlet;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import model.Movie;
import persistence.MovieDAO;

/**
 * Servlet implementation class MovieServlet
 */
@WebServlet(urlPatterns = {"/movies","/movies/*"})
public class MovieServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public MovieServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		//response.getWriter().append("Served at: ").append(request.getContextPath());
		String []url = request.getRequestURI().split("/");
		int longitud = url.length;
		System.out.println(longitud);
		if(longitud == 3){
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/view/index.jsp");
			dispatcher.forward(request, response);
		} else if (longitud > 3){
			String piece = url[3];
			if("index".equals(piece)){
				index(request, response);
			} else if (piece.equals("create")){
				create(request, response);
			} 
			int id = 0;
			System.out.println("entrando");

			if (longitud > 4) {
				try {
					id = Integer.parseInt(piece);

					System.out.println("entrando id");
				} catch (Exception e) {
					response.sendRedirect(request.getContextPath());
				}
				piece = url[4];                        
				if (piece.equals("delete")){
					System.out.println("entrando a delete");
					delete(request, response, id);
					return;                        
				} else if(piece.equals("remember")){
					remember(request, response, id);
				}
			}
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String []url = request.getRequestURI().split("/");

		int longitud = url.length;

		if (longitud == 4) {

			store(request, response);
		}
	}

	private void create(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/view/movies/create.jsp");
		dispatcher.forward(request, response);
	}

	private void index(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/view/movies/index.jsp");

		
		ArrayList<Movie> movies = null;
		try {
			MovieDAO dao = new MovieDAO();
			movies = dao.all();
		} catch (SQLException ex) {
			// TODO: handle exception
			System.out.println("Fallo en articles.all()");
			System.out.println("SQLException: " + ex.getMessage());
		}		        
		request.setAttribute("list", movies);

		
		dispatcher.forward(request, response);
	}

	private void store(HttpServletRequest request, HttpServletResponse response) throws IOException {
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/view/movies/index.jsp");
		Movie movie = new Movie();
		movie.setTitle(request.getParameter("title"));
		movie.setDirector(request.getParameter("director"));
		try{
			movie.setYear(Integer.parseInt(request.getParameter("year")));
		}catch (NumberFormatException e){
			System.out.println(e.getMessage());
			movie.setYear(0);
		}
		MovieDAO insertarMovieBD = new MovieDAO();
		insertarMovieBD.insert(movie);
		response.sendRedirect(request.getContextPath() + "/movies/index");
	}

	private void remember(HttpServletRequest request, HttpServletResponse response, int id) throws IOException, ServletException {

		HttpSession session = request.getSession(true);
		

		if (session.getAttribute("list") == null) {

			session.setAttribute("list", new ArrayList<String>());
		}
		
		MovieDAO buscarPorId = new MovieDAO();
		Movie movieSession = buscarPorId.get(id);
		String nombre = movieSession.getTitle();
		ArrayList<String> miArray = (ArrayList<String>) session.getAttribute("list");
		miArray.add(nombre);
		session.setAttribute("list", miArray);
		
		System.out.println("AGREGADO A LA SESION CORRECTAMENTE");
		
		for (String string : miArray) {
			System.out.println(string);
		}

		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/view/footer.jsp");
		
		response.sendRedirect(request.getContextPath() + "/movies/index");
		
	}


	private void delete(HttpServletRequest request, HttpServletResponse response, int id) throws IOException {
		
		//Delete realizado a medias.
		System.out.println(id);
		MovieDAO eliminarMovie = new MovieDAO();
		eliminarMovie.delete(id);
		response.sendRedirect(request.getContextPath() + "/movies/index");
		return;
	}
}
